package com.springdemo;

public class CricketCoach implements Coach {
	
	private FortuneService fortuneService;
	private String emailAddress;
	private String team;
	
	public CricketCoach() {
		System.out.println("inside cricket coach");
	}
	
	public void setFortuneService(FortuneService theFortuneService){
		this.fortuneService = theFortuneService;
	}
	
	public void setEmailAddress(String theEmail){
		this.emailAddress = theEmail;
	}
	
	public void setTeam(String theTeam){
		this.team = theTeam;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public String getTeam() {
		// TODO Auto-generated method stub
		return team;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Practice fast bowling for 15 mins";
	}

	@Override
	public String getDailyFortunes() {
		return fortuneService.getFortune();
	}



}
