package com.springdemo;

public class TrackCoach implements Coach {
	
	private FortuneService fortuneService;
	
	public TrackCoach (FortuneService theFortuneService){
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Run a hard 5km";
	}

	@Override
	public String getDailyFortunes() {
		return fortuneService.getFortune();
	}
	
	public void doMyStartUpStuff (){
		System.out.println("inside method doMyStartUpStuff");
	}
	
	public void doMyCleanUpStuff(){
		System.out.println("inside method doMyCleanUpStuff");
	}


}
